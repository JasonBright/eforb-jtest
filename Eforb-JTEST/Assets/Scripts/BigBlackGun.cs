﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBlackGun : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown( 0 ))
        {
            var screenPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var hit = Physics2D.Raycast(screenPos, Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.transform.tag == "Enemy")
                {
                    Destroy(hit.transform.gameObject);
                }
            }
        }
    }
}
