﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    private Vector3 vector;
    private float speed;

    void Start()
    {
        vector = new Vector3( Random.Range( -1.0F, 1.0F ), Random.Range( -1.0F, 1.0F ) );
        speed = Random.Range( 0.0F, 5.3F );
    }

    void Update()
    {
        transform.Translate( vector * speed * Time.deltaTime );
        var bounds = RAMKI();
        if (bounds.min.x > transform.position.x || bounds.max.x < transform.position.x ||
            bounds.max.y > transform.position.y || bounds.min.y  < transform.position.y)
        {
            Destroy( gameObject );
        }
    }

    public Bounds RAMKI()
    {
        return (Bounds)typeof (EnemySpawner).GetMethod( "ПолучитьРамкиЭкрана", BindingFlags.NonPublic | BindingFlags.Instance ).Invoke( FindObjectOfType<EnemySpawner>(), null );
    }
}
