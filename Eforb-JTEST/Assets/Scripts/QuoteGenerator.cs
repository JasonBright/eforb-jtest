﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuoteGenerator : MonoBehaviour
{
    public string[] ARRAY;

    void OnEnable()
    {
        GetComponent<Text>().text = ARRAY[Random.Range( 0, ARRAY.Length )];
    }
}
