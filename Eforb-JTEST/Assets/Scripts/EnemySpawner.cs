﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject ENEMY_PREFAB;

    [SerializeField] private Vector2 _delayDiapason = new Vector2(0.5F, 1);

    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(_delayDiapason.x, _delayDiapason.y));
            SpawnEnemy();
        }
        
    }

    public void SpawnEnemy()
    {
        var enemy = Instantiate( ENEMY_PREFAB );
        enemy.transform.position = new Vector3(
            Random.Range( ПолучитьРамкиЭкрана().min.x, ПолучитьРамкиЭкрана().max.x ),
            Random.Range( ПолучитьРамкиЭкрана().min.y, ПолучитьРамкиЭкрана().max.y ) 
        );
    }

    private Bounds ПолучитьРамкиЭкрана()
    {
        var upperLeft = Camera.main.ScreenToWorldPoint( new Vector3( -Screen.width/2 * 0.85F, 0 ) );
        var lowerRight = Camera.main.ScreenToWorldPoint( new Vector3( Screen.width/2 * 0.8F, Screen.height * 0.81F ) );
        return new Bounds( Camera.main.transform.position, 
            new Vector3( lowerRight.x - upperLeft.x, upperLeft.y - lowerRight.y, 100 ) );
    }
}
